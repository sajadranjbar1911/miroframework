<?php

use \Core\Routes\Routes;

class Router
{

    public static function route($url, $request)
    {
        if ($url !== "") {
            //Explode Url
            $url = explode("/", $url);
            //set the rout's name
            $name = strtolower($url[0]);

            //check exists rout's name in routes array
            if (!Routes::hasRout($name)) {
                $request->not_found = true;
                return;
            }
            $route = Routes::$routes[$name];
            //Check Http Request Method
            if ($_SERVER['REQUEST_METHOD'] != strtoupper($route["http"])) {
                $request->not_found = true;
                return;
            }
            //Set Controller, Method and params
            $request->Controller = $route["controller"];
            $request->Method = $route["method"];
            $request->Params = array_slice($url, 1);

        }

    }

}

