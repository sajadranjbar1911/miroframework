<?php
//Auto loading classes
spl_autoload_register();

//Set All Basic And Dynamic Constants
$definer = new \Core\Config\Definer();

//Include the core
include(BASE_DIR . "Core/Controller.php");
include(BASE_DIR . "Core/Model.php");

//Call The Request
$request = new Request();

//Call The Dispatcher
$dispatch = new Dispatcher($request);
////Call The Dispatcher
$dispatch->dispatch();