<?php


namespace Core;


use Core\Traits\Log;

class Controller
{
    use Log;
    public $valuable;

    /**
     * @param mixed $valuale
     */
    public function setValuable($value)
    {
        $this->valuable = array_merge($value);
    }

    /*
     * Render the html Page
     * */
    public function loadView($page, $values = [])
    {
        $this->setValuable($values);
        extract($values);
        ob_start();

        $view_file = BASE_DIR . "App/Views/" . $page . "View.php";

        if (!file_exists($view_file))
            return false;

        $this->setLog($_SERVER['REMOTE_ADDR'], $page);

        include $view_file;

    }

    public function loadModel($model)
    {
        $model_file = BASE_DIR . "App/Models/" . $model . ".php";
        //Check Model Exists
        if (!file_exists($model_file))
            return false;
        //Fully Qualified Model Name
        $model = "App\Models\\" . $model;
        var_dump( $model);
        die();
        return new $model;
    }
}