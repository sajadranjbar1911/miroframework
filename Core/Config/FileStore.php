<?php


namespace Core\Config;


use Core\Interfaces\FileInterface;

class FileStore implements FileInterface
{

    public function create($handler, $data)
    {
        fwrite($handler, $data);
    }

    public function delete($handler, $condition)
    {
        unlink($handler);
    }

    public function connection($dir, $mode)
    {
        return $handler = fopen(BASE_DIR . "Storage/File/" . $dir, "$mode");
    }

    public function read($handler, $condition = "")
    {
        while (!feof($handler)) {
            $line = fgets($handler);

            if ($line[0][0] !== "" && $line[0][0] !== "/") {
                $lines[] = $line;
            }
        }

        return $lines;
    }

}