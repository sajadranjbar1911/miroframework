<?php

namespace Core\Config;
class Definer
{
    public function __construct()
    {
        $base_dir = trim(str_replace("Index.php", "", $_SERVER['SCRIPT_FILENAME']));
        define('BASE_DIR', $base_dir);

        $base_url = trim(str_replace("index.php", "", $_SERVER['SCRIPT_NAME']), "/");
        $base_url = trim(str_replace("/", "\\", $base_url));

        define('BASE_URL', $base_url);


        $const_dir = BASE_DIR . ".const";

        if (!file_exists($const_dir))
            return false;

        $file = fopen($const_dir, "r");

        while (!feof($file)) {
            $const = fgets($file);

            if ($const[0] !== "" && $const[0] !== "/") {

                $define = explode("=", $const);
                define(trim($define[0]), trim($define[1]));
            }
        }

    }

}