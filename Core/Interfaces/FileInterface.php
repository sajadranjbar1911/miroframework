<?php


namespace Core\Interfaces;


interface FileInterface extends CreateData, DeleteData, ReadData
{
    public function connection($dir, $mode);
}