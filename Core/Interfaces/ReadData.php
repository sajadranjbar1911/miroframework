<?php


namespace Core\Interfaces;


interface ReadData
{
    public function read($handler, $condition);
}