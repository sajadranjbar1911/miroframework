<?php


namespace Core\Interfaces;


interface DeleteData
{
    public function delete($handler, $condition);
}