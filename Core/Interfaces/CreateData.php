<?php


namespace Core\Interfaces;


interface CreateData
{
    public function create($handler, $data);
}