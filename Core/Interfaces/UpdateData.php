<?php


namespace Core\Interfaces;


interface UpdateData
{
    public function update($handler, $data, $condition);
}