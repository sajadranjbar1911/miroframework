<?php


namespace Core\Routes;


use Router;

class Routes
{
    public static $routes = array();

    public static function addRoute($name, $path)
    {
        self::$routes = array_merge(self::$routes, ["$name" => $path]);
    }

    public static function hasRout($url)
    {
        return array_key_exists($url, self::$routes);
    }

}


Routes::addRoute("hello", ["http" => "GET", "controller" => "Welcome", "method" => "index"]);
Routes::addRoute("print", ["http" => "GET", "controller" => "Welcome", "method" => "printer"]);
Routes::addRoute("showalluser", ["http" => "GET", "controller" => "Welcome", "method" => "showAllUsers"]);

