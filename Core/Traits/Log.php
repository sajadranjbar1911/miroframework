<?php


namespace Core\Traits;

session_start();


trait Log
{
    private function setLog($user_ip, $page)
    {
        //set SESSION for path log
        if (!isset($_SESSION["LOG"])) {
            $_SESSION["LOG"] = $page . "View.php ";
        } else {
            $_SESSION["LOG"] .= "=>" . $page . "View.php ";
        }

        //set time zone to tehran
        date_default_timezone_set("Asia/tehran");
        $time = date("h:i:s");

        //set date for file name
        $today_date = date("Y-m-d");

        //set useragent for log
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        //Address file
        $file_dir = LOG_PATH . $today_date . ".txt";

        //open or creat file
        $file = fopen($file_dir, "a+");

        /*
         *  write new log
         * if exist log clear and write new log
         */
        while (!feof($file)) {

            $line = fgets($file);
            if (strpos($line, $user_ip)) {
                $content = file_get_contents($file_dir);
                $content = str_replace($line, "", $content);
                file_put_contents($file_dir, $content);

            }

        }

        $log = "IP : " . $user_ip . " ** USER_AGENT : " . $user_agent . " ** TIME : " . $time .
            " ** THE ROUTE HAS GONE : " . $_SESSION["LOG"] . "\r\n";
        fwrite($file, $log);

        fclose($file);
    }
}