<?php


class Request
{
    //Url form is: protocol://domain.tld/somename/params
    public $url;
    //Set Default Controller , Method and Parameters
    public $Controller = "Welcome";
    public $Method = "index";
    public $Params = [];

    //Not Found Page
    public $not_found = false;

    public function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] === "GET") {//when method is get
            $this->url = $_GET['par'];
        } elseif ($_SERVER['REQUEST_METHOD'] === "POST") {//when method is post
            $this->url = $_POST["con"];
        }
    }

}