<?php


class Dispatcher
{
    public $request;
    public $url;

    public function __construct($request)
    {
        //Set request and url
        $this->request = $request;
        $this->url = $request->url;

    }

    public function dispatch()
    {
        Router::route($this->url, $this->request);
        $controller = $this->loadController();

        //Call The Controller , The Method By Params
        call_user_func_array([$controller, $this->request->Method], [$this->request->Params]);

    }

    public function loadController()
    {
        //Check Conditional Not Found Controller Flag
        $controller = $this->request->not_found ? "NotFound" : $this->request->Controller;

        //The Controller File Address
        $con_file = BASE_DIR . "App/Controllers/" . $controller . "Controller.php";

        include($con_file);

        //The Controller fully Qualified Name
        $controller = "\App\Controllers\\" . $controller . "Controller";
        return new $controller;
    }

}