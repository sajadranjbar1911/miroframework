<?php


namespace App\Models;


use Core\Config\FileStore;

class UserFile extends FileStore
{
    public $dir = "UserFile";
    public $handler;

    public function __construct()
    {
        $this->handler = $this->connection($this->dir, "r+");
    }

    public function readUser()
    {
        return $this->read($this->handler);
    }

    public function writeUser($data)
    {
        $this->create($this->handler, $data);
    }
}