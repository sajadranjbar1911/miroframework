<?php


namespace App\Controllers;


use Core\Controller;

class WelcomeController extends Controller
{

    public function index()
    {
        $this->loadView("Welcome");
    }

    public function printer($value)
    {

        $this->loadView("Printer", $value);
    }

    public function showAllUsers()
    {

        $model = $this->loadModel("UserFile");
        $users = $model->readUser();

        $this->loadView("ShowUser", $users);
    }
}