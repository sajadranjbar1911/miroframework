<?php


namespace App\Controllers;


use Core\Controller;

class NotFoundController extends Controller
{
    public function index()
    {
        $this->loadView("Error404");
    }

}