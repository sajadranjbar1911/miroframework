<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User</title>
</head>
<body style="text-align-last: center; padding-top: 100px;">
<h1>List of all users</h1>
<ul>
    <?php
    foreach ($values as $user)
        echo "<li>" . $user . "</li>";
    ?>
</ul>
<a href="/<?= BASE_URL ?>"> Welcome </a>
</body>
</html>