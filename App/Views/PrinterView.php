<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome Page</title>
</head>
<body style="text-align-last: center; margin-top: 100px">
<h1>Printer Page</h1>
<h3>You Can Print Everythings That You Want</h3>

<div style="border:solid 2px black; border-radius: 10px; padding: 10px; margin: 10px;">
    <ul>
        <?php
        foreach ($values as $value) {
            echo "<li>" . $value . "</li>";
        }
        ?>
    </ul>
</div>
<a href="/<?= BASE_URL ?>"> Welcome </a>

</body>
</html>